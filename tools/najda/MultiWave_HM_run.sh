#!/bin/sh

if [ $# -ne 2 ] 
then
  echo USAGE : $0 VERSION MAX_WAVE
  exit 1
fi

vers=$1
maxw=$2

for i in `seq -w 1 $maxw`
do
  ./OneWave_HM_config.sh $vers $i

  cd /home/villefranquen/Work/R/SCRIPTS/
  Rscript $1_convertCnrmDesigns.R
  
  # remove ' from ascii file
  sed -i 's/"//g' $(ls -t /home/villefranquen/Work/MUSC/Par1D_*asc | head -1)

  cd /home/villefranquen/Work/MUSC/namelist/ARPEGE/tuning/
  cdat prep_config.py
  cdat prep_nam.py

  cd /home/villefranquen/Work/MUSC/
  ./run_tuning.sh

  cd /home/villefranquen/Work/R/SCRIPTS/
  Rscript $1_ncdf2Rdata.R

  Rscript $1_buildEmulator_runOneWave.R
done
