#!/bin/sh
if [ "$#" -ne 2 ] ; then
  echo "Usage : $0 VERSION NWAVE" >&2
  exit 1 
fi


# SCRIPT VERSION
vers=$1

# LES DATA
PNCDF="\/home\/villefranquen\/Work\/NCDF\/ARMCU\/SENSIB_MEAN\/"
NLESS=11  
LESSI='c("CERK4","CEN2D","D100m","DX40m","DZ40m","DZVAR","L10km","L25km","TDELT","WENO3","WENO5")'

# 1D DATA
NPARA=9
NRUNS=160
PMUSC='\/home\/villefranquen\/Work\/MUSC\/'
DMUSC='/home/villefranquen/Work/MUSC/'
MODEL=cnrm

# CASE DATA
NCASE=ARMCu
AVVAR='c("lwp","theta","rneb","Mf","w_up","ql","qv")'

# R DATA & HM DATA
PDATA="\/home\/villefranquen\/Work\/R\/DATA\/"
NMETR=lwp
WAVEN=$2

# Copy model scripts to configurable version
cp base_ncdf2Rdata.R /home/villefranquen/Work/R/SCRIPTS/${vers}_ncdf2Rdata.R
cp base_buildEmulator_runOneWave.R /home/villefranquen/Work/R/SCRIPTS/${vers}_buildEmulator_runOneWave.R
cp base_convertCnrmDesigns.R /home/villefranquen/Work/R/SCRIPTS/${vers}_convertCnrmDesigns.R
cp ${DMUSC}base_run_tuning.sh ${DMUSC}run_tuning.sh
cp ${DMUSC}namelist/ARPEGE/tuning/base_prep_config.py  ${DMUSC}namelist/ARPEGE/tuning/prep_config.py 
cp ${DMUSC}namelist/ARPEGE/tuning/base_prep_nam.py  ${DMUSC}namelist/ARPEGE/tuning/prep_nam.py

fich="/home/villefranquen/Work/R/SCRIPTS/${vers}_ncdf2Rdata.R /home/villefranquen/Work/R/SCRIPTS/${vers}_buildEmulator_runOneWave.R /home/villefranquen/Work/R/SCRIPTS/${vers}_convertCnrmDesigns.R  ${DMUSC}run_tuning.sh ${DMUSC}namelist/ARPEGE/tuning/prep_config.py ${DMUSC}namelist/ARPEGE/tuning/prep_nam.py"

sed -i "s/_PDATA_/'${PDATA}'/g" $fich
sed -i "s/_RDATA_/'${RDATA}'/g" $fich
sed -i "s/_NLESS_/${NLESS}/g" $fich
sed -i "s/_NPARA_/${NPARA}/g" $fich
sed -i "s/_NRUNS_/${NRUNS}/g" $fich
sed -i "s/_NCASE_/'${NCASE}'/g" $fich
sed -i "s/_NCAS_/${NCASE}/g" $fich
sed -i "s/_PMUSC_/'${PMUSC}'/g" $fich 
sed -i "s/_PNCDF_/'${PNCDF}'/g" $fich
sed -i "s/_AVVAR_/${AVVAR}/g" $fich
sed -i "s/_LESSI_/${LESSI}/g" $fich
sed -i "s/_WAVEN_/${WAVEN}/g" $fich
sed -i "s/_MODEL_/'${MODEL}'/g" $fich
sed -i "s/_MODL_/${MODEL}/g" $fich
sed -i "s/_NMETR_/'${NMETR}'/g" $fich
