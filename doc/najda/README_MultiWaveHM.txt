HOW TO USE R SCRIPTS TO DO HISTORY MATCHING 

1. Run LES simulations as observations : a reference simulation + ($NLESS-1) sensitivity tests

2. Chose $Nparams sensible parameters to tune (inputs of History Matching)

3. Sample hypercube [-1 , 1]^Nparams (first wave) or NROYn (wave n+1) space and convert samples to true parameter values
===> ConvertCnrmDesigns.R

5. Run NRUNS times the 1D model (CNRM or LMD)

6.1 Read the NRUNS ncdf output files
6.2 Chose a metric to compute and specify the get_metric function
6.3 Generate RData object with tData (param sample, 1D output, metric name, metric index in tData, LES obs, LES variability) and save to file
===> ncdf2Rdata.R

7.1 Build an emulator from a tData object that you can load from 
7.2 Generate NROY space with emulator and LES obs 
===> buildEmulator_runeOneWave.R

8. Back to 3 to start a new wave  


OR 

1. Run LES simulations as observations : a reference simulation + ($NLESS-1) sensitivity tests

2. Chose $Nparams sensible parameters to tune (inputs of History Matching)

3. Open OneWave_HM_config.sh
   and edit it to match your experiment set up :
 
   # LES DATA config for wave 1 you need to compute your observed metric (from LES simulations) and associated error 
   PNCDF= a path to the directory containing LES simulations for your case 
   NLESS= a number of LES sensitivity test simulations available in that directory  
   LESSI= a c() list with the id of each LES simulation

   # 1D DATA config for 1D simulations of current wave 
   NPARA= the number of parameters to use as inputs for history matching routine
   NRUNS= the size of your 1D run ensemble 
   PMUSC= a path to MUSC file where you will find 1D simulations and store sampled parameters files
   DMUSC= the same path but without the \ before the / 
   MODEL= the name of the model (e.g. cnrm or lmdz)

   # CASE DATA config your case
   NCASE= the name of the case you will run (e.g. ARMCu or AYOTTE)
   AVVAR= a c() list with available outputs in your 1D and LES simulations typically : 'c("lwp","theta","rneb","Mf","w_up","ql","qv")'

   # R DATA & HM DATA config for you HM routine
   PDATA= the path to a directory where you will read and store RData files (parameters, tData, NewDesign, LESobs ...)
   NMETR= the name of the metric we will use (e.g. theta500m , maxlwp)
   WAVEN= the number of the wave you will process (by default this is the 2nd argument of shell command ./OneWave_HM_config.sh 

4. Open MultiWave_HM_run.sh 
   change the directories path so it finds the scripts (configuration, Rscripts and 1D model scripts)

5. Run MultiWave_HM_run.sh VERS MAX_WAVE
   it will loop from 1 to MAX_WAVE
   |  it will run the config file you previously set with the VERS version (for example ARMCu_theta500m)
   |  it will generate a parameter sample 
   |  it will run multiple 1D simulations with theses parameters
   |  it will transform ncdf output into RData with the metric you decided
   |  it will build an emulator with Stan functions
   |  it will find the NROY space with this emulator and some LES obs
   |  it will generate some more parameters in this NROY space
   ___
