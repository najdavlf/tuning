This GIT project is a HIGH TUNE project for the automatic tuning of GCMs via comparaison of LES and SCM simulations

The directory tools  contains the scripts and demos developed by D. Wiliamson and then by CNRM and LMD teams
The directory doc    contains text files to describe the tools 
The directory biblio contains litterature on relevant subjects (history matching, gaussian process, sampling...)
